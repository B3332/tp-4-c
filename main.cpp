#include <cstdlib>
#include "Graphe.h"
#include <iostream>
#include <string>
using namespace std;

static void testUnitaires()
{
    cout << " == DEBUT testUnitaires == " << endl;

    cout << " MAIN => Création d'une Heure" << endl;
    Heure monHeure1 = Heure(5, 25, 2);

    cout << " MAIN => Création d'un graphe" << endl;
    Graphe monGraphe;

    cout << " MAIN => Ajout 1 ressource : index.html " << endl;
    monGraphe.ajoutRessource("index.html");

    cout << " MAIN => Ajout 3 referer vers index.html" << endl;
    monGraphe.ajoutRessourceReferer("referer.html", "index.html", monHeure1);
    monGraphe.ajoutRessourceReferer("referer.html", "index.html", monHeure1);
    monGraphe.ajoutRessourceReferer("referer.html", "index.html", monHeure1);

    cout << " MAIN => Ajout 1 ressource : index.html " << endl;
    monGraphe.ajoutRessource("index.html");

    cout << " MAIN => Ajout 1 referer : index.html " << endl;
    monGraphe.ajoutRessourceReferer("referer2.html", "index.html", monHeure1);

    cout << " MAIN => Ajout 2 ressource : index.html " << endl;
    monGraphe.ajoutRessource("index.html");
    monGraphe.ajoutRessource("index.html");

    cout << " MAIN => Ajout 1 ressource : index2.html " << endl;
    monGraphe.ajoutRessource("index2.html");

    cout << " == FIN testUnitaires == " << endl;
}

static void testLogSimple()
{
    cout << " == DEBUT testLogSimple == " << endl;

    cout << " MAIN => Création d'un graphe" << endl;
    Graphe monGraphe;

    cout << " MAIN => Chargement d'un fichier de logs" << endl;
    monGraphe.ChargerFichierLogs("test2.log");

    cout << " == FIN testLogSimple == " << endl;
}

static void testHeure()
{
    cout << " == DEBUT testHeure == " << endl;

    cout << " MAIN => Création d'une Heure" << endl;
    Heure monHeure1 = Heure(5, 25, 2);
    Heure monHeure2 = Heure(5, 25, 3);
    cout << monHeure1 << endl;
    cout << monHeure2 << endl;
    cout << "1 < 2 ? " << (monHeure1 < monHeure2) << endl;
    cout << "2 < 1 ? " << (monHeure2 < monHeure1) << endl;

    cout << " == FIN testHeure == " << endl;
}

static void testConfiguration()
{
    Graphe monGraphe;
    monGraphe.chargerConfig();
}

static int traitementArgs(int argc, char** argv)
// Fonction initiale permettant le parsing des arguments et le lancement des bonnes
//ressources associées.
{

#ifdef MAP
    clog << "main : nombre Arguments : " << argc << endl;
#endif

    if (argc > 1)
    {
        string arg;

        //Variables utiles
        int heure;
        Heure heureDebut = NO_HEURE;
        bool verifType = false;
        bool verifHeure = false;
        bool creerFichierGraphViz = false;
        int nbArgumentsUtilises = 0;

        string nomFichierGraphViz;
        string nomFichierLog;

        //Création du graphe
        Graphe monGraphe;
        monGraphe.chargerConfig();

        //Gestion des arguments, parsing
        for (int i = 1; i < argc; i++)
        {
            arg = string(argv[i]);
#ifdef MAP
            clog << "main : arg courant : " << arg << endl;
#endif

            if (arg == "-t")
            {
#ifdef MAP
                clog << "main : Arguments -t levé. " << endl;
#endif
                // Vérifier si l'argument suivant existe !
                try
                {
                    heure = stoi(string(argv[i + 1]));
                    if (0 <= heure && heure < 24)
                    {
                        verifHeure = true;
                        heureDebut = Heure(heure, 0, 0);
                    }
                    else
                    {
                        std::cerr << "ERREUR: Vous n'avez pas fourni d'heure valide" << endl;
                    }
                    nbArgumentsUtilises = nbArgumentsUtilises + 2;

                }
                catch (const std::exception & Exp)
                {
                    nbArgumentsUtilises = nbArgumentsUtilises + 1;
                    std::cerr << "ERREUR: Vous n'avez pas fourni d'heure" << endl;
                }
            }

            if (arg == "-e")
            {
#ifdef MAP
                clog << "main : Arguments -e levé. " << endl;
#endif
                verifType = true;
                nbArgumentsUtilises = nbArgumentsUtilises + 1;
            }

            if (arg == "-g")
            {
#ifdef MAP
                clog << "main : Arguments -g levé. " << endl;
#endif
                try
                {
                    //Vérification de la présence du nom du fichier GraphViz
                    if (i + 1 <= argc) nomFichierGraphViz = string(argv[i + 1]);
                    creerFichierGraphViz = true;
                    nbArgumentsUtilises = nbArgumentsUtilises + 2;
                }
                catch (const std::exception & Exp)
                {
                    nbArgumentsUtilises = nbArgumentsUtilises + 1;
                    cerr << "ERREUR: Le nom du fichier GraphViz n'est pas spécifié"
                            << endl;
                }

            }
        }

#ifdef MAP
        clog << " Nombre Argument utilisés : " << nbArgumentsUtilises << " et argc : "
                << argc << endl;
#endif

        nomFichierLog = string(argv[argc - 1]);
        if (nomFichierLog != "" && (argc - 2) != nbArgumentsUtilises)
        {
            cerr << "ERREUR: Le nom du fichier de logs n'est pas spécifié" << endl;
        }
        else
        {

            //On charge le fichier avec les bons paramètres et si tout est ok, on continue.
            if(monGraphe.ChargerFichierLogs(nomFichierLog, heureDebut, verifType)){
                // Si on doit créer le fichier
                if (creerFichierGraphViz)
                {
                    monGraphe.GenererFichierGraph(nomFichierGraphViz);
                    cout << "Dot-file " << nomFichierGraphViz << " generated" << endl;

                }
                Heure hTmp = heureDebut + Ecart_Param_T;
                if (verifHeure) cout << "Warning : only hits between " << heureDebut.getHeure()
                        << "h and " << hTmp.getHeure() << "h have been taken into account" << endl;
                monGraphe.AfficherTop(10);
            }

#ifdef TIMEMAP
            clog << "\n==== AFFICHAGE COMPLET DE LA MAP ====" << endl;
            monGraphe.AfficherTop(100000);
#endif

        }

    }
    else
    {
        cerr << "Erreur, nombre d'argument(s) invalide" << endl;
    }

    return 0;
}

int main(int argc, char** argv)
{
    //testHeure();
    //testUnitaires();
    //testLogSimple();
    //testConfiguration( );
    traitementArgs(argc, argv);

    return 0;
}
