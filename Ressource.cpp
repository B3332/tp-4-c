/*************************************************************************
                           Ressource  -  description
                             -------------------
    début                : Janvier - Février 2017
    copyright            : (C) 2016-2017 par FALCONIERI VINCENT - GIRAUD CORENTIN
    e-mail               : vincent.falconieri@insa-lyon.fr & corentin.giraud@insa-lyon.fr
 *************************************************************************/
//---------- Réalisation de la classe <Ressource> (fichier Ressource.cpp) ------------
//---------------------------------------------------------------- INCLUDE
//-------------------------------------------------------- Include système
using namespace std;
#include <iostream>
//------------------------------------------------------ Include personnel
#include "Ressource.h"
//------------------------------------------------------------- Constantes
//---------------------------------------------------- Variables de classe
//----------------------------------------------------------- Types privés
//----------------------------------------------------------------- PUBLIC
//-------------------------------------------------------- Fonctions amies
//----------------------------------------------------- Méthodes publiques
bool Ressource::ajoutLien(string URLPageCible, Heure heure)
// Algorithme : aucun
{
#ifdef MAP
    clog << "Ressource : méthode ajoutLien - Début - Page Cible : " << URLPageCible << endl;
#endif

    //On créé un itérateur
    listeLiensSortTYPE::iterator it;
    bool answer = false;

    //On vérifie si URL existe dans la base des liens
    it = listeLiensSortants.find(URLPageCible);

    //Si on a touché la fin de la map, la ressource n'est pas présente.
    if (it == listeLiensSortants.end())
    {
#ifdef MAP
        clog << "Ressource : méthode ajoutLien - Le lien n'existe pas. On l'ajoute. " << endl;
#endif
        //Insert renvoie une paire. On en prépare donc une pour retenir
        std::pair < listeLiensSortTYPE::iterator, bool> retour;

        //On la rajoute (Appelle double au constructeur par copie de Ressource, pourrait être amélioré)
        retour = listeLiensSortants.insert(make_pair(URLPageCible, Lien()));
#ifdef MAP
        clog << "Ressource : méthode ajoutLien - Le lien ajouté est : " << endl;
        clog << retour.first->second << endl;
#endif
        //On récupère si l'ajout a été fait correctement
        answer = retour.second;
    }
    else //Elle est déjà présente.
    {
#ifdef MAP
        clog << "Ressource : méthode ajoutLien - Le lien existe. On lui indique " <<
                "qu'elle a 1 utilisation en plus. Précédemment : " << endl;
        clog << it->second << endl;
#endif
        it->second.ajoutUtilisation(heure);
    }
    return answer;
} //----- Fin de ajoutLien

string Ressource::getType(string URL)
// Algorithme : aucun
{
#ifdef MAP
    clog << "Ressource : getType - Parsing de l'URL : " << URL << " pour trouver le type." << endl;
#endif

    std::size_t found = URL.find_last_of(".");
    string type = URL.substr(found + 1);

#ifdef MAP
    clog << "Ressource : getType - type parsé: " << type << endl;
#endif
    return type;
}//----- Fin de getType

bool Ressource::ajoutVue()
// Algorithme : aucun
{
    this->nbVue++;
    return true;
} //----- Fin de ajoutVue

listeLiensSortTYPE & Ressource::getListeLiensSortants()
// Algorithme : aucun
{
    return listeLiensSortants;
}//----- Fin de getlisteLiensSortants

unsigned int Ressource::getIndex() const
// Algorithme : aucun
{
    return index;
}//----- Fin de getlisteLiensSortants

unsigned int Ressource::getnbVue() const
// Algorithme : aucun
{
    return nbVue;
}//----- Fin de getlisteLiensSortants

//------------------------------------------------- Surcharge d'opérateurs
ostream & operator<<(ostream & out, const Ressource & variable)
{
    out << "Ressource de type " << variable.typeName << " et a " << variable.nbVue << " vue(s).";
    
    // ITERATOR sur : unordered_map<<string, Lien> listeLiensSortants;
    std::unordered_map<string, Lien>::const_iterator itest;
    if(variable.listeLiensSortants.begin()!=variable.listeLiensSortants.end()){
         out << endl;
    }
    for (itest = variable.listeLiensSortants.begin(); 
            itest != variable.listeLiensSortants.end(); ++itest)
    {
        cout << "cible : " << itest->first << endl;
        cout << itest->second << endl;
    }

    return out;
}//----- Fin de operator<<

//-------------------------------------------- Constructeurs - destructeur

Ressource::Ressource(const Ressource & unRessource)
// Algorithme : aucun
{
#ifdef MAPV
    clog << "Appel au constructeur de copie de <Ressource>" << endl;
#endif
    this->typeName = unRessource.typeName;
    this->index = unRessource.index;
    this->nbVue = unRessource.nbVue;
    this->listeLiensSortants = unRessource.listeLiensSortants;
} //----- Fin de Ressource (constructeur de copie)

Ressource::Ressource(string URL, unsigned int index)
// Algorithme : aucun
{
#ifdef MAPV
    clog << "Appel au constructeur de <Ressource>" << endl;
#endif

    //Parsing de l'URL
    this->typeName = getType(URL);
    this->index = index;
    this->nbVue = 0;
} //----- Fin de Ressource

Ressource::~Ressource()
// Algorithme : aucun
{
#ifdef MAPV
    clog << "Appel au destructeur de <Ressource>" << endl;
#endif
} //----- Fin de ~RessourcegetListeLiensSortants

//------------------------------------------------------------------ PRIVE
//----------------------------------------------------- Méthodes protégées
//------------------------------------------------------- Méthodes privées
//------------------------------------------------------- Méthodes privées
