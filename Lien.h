/*************************************************************************
                           Lien  -  description
                             -------------------
    début                : Janvier - Février 2017
    copyright            : (C) 2016-2017 par FALCONIERI VINCENT - GIRAUD CORENTIN
    e-mail               : vincent.falconieri@insa-lyon.fr & corentin.giraud@insa-lyon.fr
 *************************************************************************/
//---------- Interface de la classe <Lien> (fichier Lien.h) ----------------
#if ! defined ( LIEN_H )
#define LIEN_H

//--------------------------------------------------- Interfaces utilisées
#include "Heure.h"
//------------------------------------------------------------- Constantes
const Heure NO_HEURE = Heure(-1, 0, 0);
//------------------------------------------------------------------ Types
//------------------------------------------------------------------------
// Rôle de la classe <Lien>
// Retenir des informations basique sur un lien entre deux pages.
// Connaître le nombre de fois que ce lien a été utilisé, etc.
//------------------------------------------------------------------------

class Lien {
    //----------------------------------------------------------------- PUBLIC
public:
    //----------------------------------------------------- Méthodes publiques
    bool ajoutUtilisation(Heure heure = NO_HEURE);
    // Permet d'incémenter le compteur d'utilisation et de stocker l'heure
    // d'utilisation de ce lien.
    // Mode d'emploi :
    // Heure : l'heure à laquelle le lien a été utilisé
    // Contrat : aucun

    int getnbUtilisation() const;
    // Permet de retourner combien de fois le lien a été utilisé
    // Mode d'emploi : aucun
    // Contrat : aucun

    //------------------------------------------------- Surcharge d'opérateurs
    Lien & operator=(const Lien & unLien);
    // Surcharge de l'opérateur d'égalité. 

    friend ostream & operator<<(ostream & out, const Lien & variable);
    // Surcharge de l'opérateur de sortie. 

    //-------------------------------------------- Constructeurs - destructeur
    Lien(const Lien & unLien);
    // constructeur de copie 
    // Contrat : pas de contrat

    Lien(int nbUtilisations = 1);
    // Constructeur de la classe Lien
    // Mode d'emploi :
    // Le nombre de fois où le lien a été utilisé. Par défaut, une seule fois.
    // Contrat : aucun

    virtual ~Lien();
    // Destructeur de la classe Lien

    //------------------------------------------------------------------ PRIVE
protected:
    //----------------------------------------------------- Méthodes protégées
private:
    //------------------------------------------------------- Méthodes privées
protected:
    //----------------------------------------------------- Attributs protégés
private:
    //------------------------------------------------------- Attributs privés
    int nbUtilisations;
    //Le nombre de hits de chaque lien.
    //---------------------------------------------------------- Classes amies
    //-------------------------------------------------------- Classes privées
    //----------------------------------------------------------- Types privés
};
//---------------------------------------------- Types dépendants de <Lien>
#endif // LIEN_H