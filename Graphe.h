/*************************************************************************
                           Graphe  -  description
                             -------------------
    début                : Janvier - Février 2017
    copyright            : (C) 2016-2017 par FALCONIERI VINCENT - GIRAUD CORENTIN
    e-mail               : vincent.falconieri@insa-lyon.fr & corentin.giraud@insa-lyon.fr
 *************************************************************************/

//---------- Interface de la classe <Graphe> (fichier Graphe.h) ----------------
#if ! defined ( GRAPHE_H )
#define GRAPHE_H
using namespace std;
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <map>
#include <vector>
#include <utility>
#include <set>
#include <algorithm>
#include <functional>
#include <sys/stat.h>
//--------------------------------------------------- Interfaces utilisées
#include "Ressource.h"
#include "Heure.h"
//------------------------------------------------------------- Constantes
const string NO_CIBLE = "";
const string NOM_FICHIER_CONFIG = "config.ini";

// Valeurs par défaut
const int DEFAULT_NB_TYPE_INTERDITS = 8;
const string DEFAULT_TYPE_INTERDITS[] = {"css", "js", "bmp", "jpeg", "jpg", "png", "gif", "ico"};
const string DEFAULT_DOMAINE= "http://intranet-if.insa-lyon.fr";
const Heure DEFAULT_ECART_PARAM_T= Heure(1,0,0);

//Paramètres de la classe
static int nb_types_interdits = 7;
static vector<std::string> types_interdits;
static string Notre_domaine;
static Heure Ecart_Param_T = Heure(1,0,0);

//------------------------------------------------------------------ Types
typedef std::unordered_map<string, Ressource> listeResTYPE;
//------------------------------------------------------------------------
// Rôle de la classe <Graphe>
// Gérer l'ensemble des Ressources web nommés dans les logs.
// Gérer les actions principales de l'executable
//------------------------------------------------------------------------

class Graphe {
    //----------------------------------------------------------------- PUBLIC
public:
    //----------------------------------------------------- Méthodes publiques
    bool ChargerFichierLogs(string nomFichier, Heure horaireDepart = NO_HEURE, bool verifType = false);
    // Permet de parser un fichier de logs en entrée
    // Mode d'emploi :
    // Le nom du fichier à parser, correct et accessible.
    // L'horaire de départ, qui sera incrémenté d'une heure pour obtenir la limite haute.
    // verifType, true pour activer la vérification de type et retirer les types énoncé en constante.
    // Contrat : pas de contrat

    bool ajoutRessource(string URL);
    // Permet d'ajouter une ressource à la structure de données
    // Mode d'emploi :
    // URL de la ressource à ajouter (propre)
    // Contrat : pas de contrat

    bool ajoutRessourceReferer(string URL, string URLcible, Heure heure);
    // Permet d'ajouter une ressource à la structure de données
    // Mode d'emploi :
    // URL de la ressource à ajouter (propre)
    // URL de la ressource cible (propre)
    // L'heure de la requête
    // Contrat : pas de contrat

    bool GenererFichierGraph(const string nomFichier);
    // Permet de générer un .dot lisible par GraphViz
    // Mode d'emploi :
    // le nom du fichier de sortie.
    // Contrat : pas de contrat

    void AfficherTop(int n);
    // Permet d'afficher les n premiers sites ayant le plus grands nombre de vues
    // Mode d'emploi :
    // n, le nombre de sites à afficher.
    // Contrat : pas de contrat

    void chargerConfig();
    // Permet de charger la configuration
    // Le nom du fichier de configuration par défaut est déclaré en constante.

    //------------------------------------------------- Surcharge d'opérateurs
    Graphe & operator=(const Graphe & unGraphe);
    // Surcharge de l'opérateur d'égalité.
    // NON-DEFINIE !

    friend ostream & operator<<(ostream & out, const Graphe & variable);
    // Surcharge de l'opérateur de sortie.

    //-------------------------------------------- Constructeurs - destructeur
    Graphe(const Graphe & unGraphe);
    // Mode d'emploi (constructeur de copie) :
    // Créé une copie du Graphe passé en paramètre.
    // Contrat : pas de contrat
    // NON-DEFINIE !

    Graphe();
    // Mode d'emploi :
    // Créé un graphe vide.
    // Contrat : pas de contrat

    virtual ~Graphe();
    // Destructeur de Graphe
    // Contrat : pas de contrat

    //------------------------------------------------------------------ PRIVE
protected:
    //----------------------------------------------------- Méthodes protégées
private:
    //------------------------------------------------------- Méthodes privées
    static string normaliseURL(string URL);
    //Permet de normaliser l'URL (retirer le nom de domaine local) en referer.

    bool ressourceEstBonType(string URL);
    //Permet de vérifier si une ressource fait partie de la liste d'exclusion.
    bool fileExists(const std::string& filename);
    //Test de fichier existant

public:
    unsigned int index = 0;

protected:
    //----------------------------------------------------- Attributs protégés
private:
    //------------------------------------------------------- Attributs privés
    unordered_map<string, Ressource> listeRessources;
    //Sa structure de données pour retenir la liste des ressources
    //---------------------------------------------------------- Classes amies
    //-------------------------------------------------------- Classes privées
    //----------------------------------------------------------- Types privés
};
//---------------------------------------------- Types dépendants de <Graphe>
#endif // GRAPHE_H
