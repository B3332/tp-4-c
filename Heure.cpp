/*************************************************************************
                           Heure  -  description
                             -------------------
    début                : Janvier - Février 2017
    copyright            : (C) 2016-2017 par FALCONIERI VINCENT - GIRAUD CORENTIN
    e-mail               : vincent.falconieri@insa-lyon.fr & corentin.giraud@insa-lyon.fr
 *************************************************************************/
//---------- Réalisation de la classe <Heure> (fichier Heure.cpp) ------------
//---------------------------------------------------------------- INCLUDE
//-------------------------------------------------------- Include système
using namespace std;
#include <iostream>
//------------------------------------------------------ Include personnel
#include "Heure.h"
//------------------------------------------------------------- Constantes
//---------------------------------------------------- Variables de classe
//----------------------------------------------------------- Types privés
//----------------------------------------------------------------- PUBLIC
//-------------------------------------------------------- Fonctions amies
//----------------------------------------------------- Méthodes publiques
bool Heure::isInside(Heure borneA, Heure borneB)
// Algorithme : aucun
{
#ifdef MAP
    clog << "Heure : isInside - On compare : " << this << " aux horaires " 
            << borneA << " et " << borneB << endl;
#endif
    bool answer = false;

    if (borneA <= *this && *this < borneB)
    {
        answer = true;
    }

#ifdef MAP
    clog << "Heure : isInside - résultat : " << answer << endl;
#endif
    return answer;
} //----- Fin de isInside

int Heure::getHeure()
// Algorithme : aucun
{
#ifdef MAP
    clog << "Heure: appel du getter heure" << endl;
#endif
    return heure;
} //----- Fin de getHeure

//------------------------------------------------- Surcharge d'opérateurs

Heure & Heure::operator=(const Heure & unHeure)
// Algorithme : aucun
{
    this->heure = unHeure.heure;
    this->min = unHeure.min;
    this->sec = unHeure.sec;

    return *this;
} //----- Fin de operator =

Heure Heure::operator+(const Heure & unHeure)
// Algorithme : aucun
{
    Heure c = Heure(unHeure.heure + this->heure, unHeure.min + this->min, unHeure.sec + this->sec);
#ifdef MAP
    clog << "Heure : operator '+' - On calcul à partir de : " << *this << " et " 
            << unHeure << " result : " << c << endl;
#endif
    return c;
} //----- Fin de operator +

bool operator<(Heure const &a, Heure const& b)
// Algorithme : aucun
{
#ifdef MAP
    clog << "Heure : operator '<' - On compare : " << a << " à " << b << endl;
#endif
    bool answer = false;

    //On calcul en secondes les valeurs de chaque heure.
    if ((a.heure * 3600 + a.min * 60 + a.sec)<(b.heure * 3600 + b.min * 60 + b.sec))
    {
        answer = true;
    }

#ifdef MAP
    clog << "Heure : operator '<' - résultat : " << answer << endl;
#endif
    return answer;
} //----- Fin de operator <

bool operator<=(Heure const &a, Heure const& b)
// Algorithme : aucun
{
#ifdef MAP
    clog << "Heure : operator '<=' - On compare : " << a << " à " << b << endl;
#endif
    bool answer = false;

    //On calcul en secondes les valeurs de chaque heure.
    if ((a.heure * 3600 + a.min * 60 + a.sec)<=(b.heure * 3600 + b.min * 60 + b.sec))
    {
        answer = true;
    }

#ifdef MAP
    clog << "Heure : operator '<=' - résultat : " << answer << endl;
#endif
    return answer;
} //----- Fin de operator <=

ostream & operator<<(ostream & out, const Heure & variable)
// Algorithme : aucun
{
    out << variable.heure << "H" << variable.min << "m" << variable.sec << "s";
    return out;
}//----- Fin de operator <<

bool operator==(Heure const &a, Heure const &b)
// Algorithme : aucun
{
    bool answer = (a.heure * 3600 + a.min * 60 + a.sec) == (b.heure * 3600 + b.min * 60 + b.sec);
#ifdef MAP
    clog << "Heure : operator '==' - On compare : " << a << " à " << b 
            << " résultat " << answer << endl;
#endif
    return answer;
}//----- Fin de operator ==

//-------------------------------------------- Constructeurs - destructeur
Heure::Heure(const Heure & unHeure)
// Algorithme : aucun
{
#ifdef MAPV
    clog << "Appel au constructeur de copie de <Heure>" << endl;
#endif
    this->sec = unHeure.sec;
    this->min = unHeure.min;
    this->heure = unHeure.heure;

} //----- Fin de Heure (constructeur de copie)

Heure::Heure(int heure, int min, int sec)
// Algorithme : aucun
{
#ifdef MAPV
    clog << "Appel au constructeur de <Heure>" << endl;
#endif

    this->sec = sec;
    this->min = min;
    this->heure = heure;

} //----- Fin de Heure

Heure::~Heure()
// Algorithme : aucun
{
#ifdef MAPV
    clog << "Appel au destructeur de <Heure>" << endl;
#endif
} //----- Fin de ~Heure

//------------------------------------------------------------------ PRIVE
//----------------------------------------------------- Méthodes protégées
//------------------------------------------------------- Méthodes privées
