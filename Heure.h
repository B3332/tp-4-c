/*************************************************************************
                           Heure  -  description
                             -------------------
    début                : Janvier - Février 2017
    copyright            : (C) 2016-2017 par FALCONIERI VINCENT - GIRAUD CORENTIN
    e-mail               : vincent.falconieri@insa-lyon.fr & corentin.giraud@insa-lyon.fr
 *************************************************************************/
//---------- Interface de la classe <Heure> (fichier Heure.h) ----------------
#if ! defined ( HEURE_H )
#define HEURE_H
//--------------------------------------------------- Interfaces utilisées
//------------------------------------------------------------- Constantes
//------------------------------------------------------------------ Types
//------------------------------------------------------------------------
// Rôle de la classe <Heure>
// Structurer des horaires dans un format simple et connu
// Permet de vérifier facilement si une horaire est dans un intervalle ou non
//------------------------------------------------------------------------

class Heure {
    //----------------------------------------------------------------- PUBLIC
public:
    //----------------------------------------------------- Méthodes publiques
    bool isInside(Heure borneA, Heure borneB);
    // Permet de savoir si l'heure est situéé dans les bornes en paramètres.
    // Mode d'emploi :
    // La Borne inférieur (incluse) de la plage horaire
    // La Borne supérieur (excluse) de la plage horaire
    // Contrat : aucun

    int getHeure();
    // Renvoi la valeur de l'attribut heure pour répondre au critère d'affichage du sujet
    // Mode d'emploi :
    // Aucun
    // Contrat : aucun

    //------------------------------------------------- Surcharge d'opérateurs
    Heure & operator=(const Heure & unHeure);
    // Surcharge de l'opérateur d'égalité.

    Heure operator+(const Heure & unHeure);
    // Surcharge de l'opérateur de somme.

    friend bool operator<(Heure const &a, Heure const& b);
    // Surcharge de l'opérateur inférieur.

    friend bool operator<=(Heure const &a, Heure const& b);
    // Surcharge de l'opérateur inférieur ou égal.

    friend ostream & operator<<(ostream & out, const Heure & variable);
    // Surcharge de l'opérateur de sortie.

    friend bool operator==(Heure const &d1, Heure const &d2);
    // Surcharge de l'opérateur d'égalité.

    //-------------------------------------------- Constructeurs - destructeur
    Heure(const Heure & unHeure);
    // constructeur de copie
    // Contrat : aucun

    Heure(int heure, int min, int sec);
    // Constructeur de la classe Heure
    // Mode d'emploi :
    // Les trois valeurs décrivants une horaire : heure, minutes, secondes
    // Contrat : aucun

    virtual ~Heure();
    // Destructeur de la classe Heure
    // Contrat : aucun

    //------------------------------------------------------------------ PRIVE
protected:
    //----------------------------------------------------- Méthodes protégées
private:
    //------------------------------------------------------- Méthodes privées
protected:
    //----------------------------------------------------- Attributs protégés
private:
    //------------------------------------------------------- Attributs privés
    int sec;
    int min;
    int heure;
    //---------------------------------------------------------- Classes amies
    //-------------------------------------------------------- Classes privées
    //----------------------------------------------------------- Types privés
};
//---------------------------------------------- Types dépendants de <Heure>
#endif // HEURE_H
