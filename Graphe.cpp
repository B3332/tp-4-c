/*************************************************************************
                           Graphe  -  description
                             -------------------
    début                : Janvier - Février 2017
    copyright            : (C) 2016-2017 par FALCONIERI VINCENT - GIRAUD CORENTIN
    e-mail               : vincent.falconieri@insa-lyon.fr & corentin.giraud@insa-lyon.fr
 *************************************************************************/
//---------- Réalisation de la classe <Graphe> (fichier Graphe.cpp) ------------
//---------------------------------------------------------------- INCLUDE
//-------------------------------------------------------- Include système
using namespace std;
//------------------------------------------------------ Include personnel
#include "Graphe.h"
//------------------------------------------------------------- Constantes
//---------------------------------------------------- Variables de classe
//----------------------------------------------------------- Types privés
//----------------------------------------------------------------- PUBLIC
//-------------------------------------------------------- Fonctions amies
//----------------------------------------------------- Méthodes publiques

bool Graphe::ChargerFichierLogs(string nomFichier, Heure horaireDepart, bool verifType)
// Algorithme : pas d'algo particulier
{
#ifdef MAP
    clog << "Graphe : méthode ChargerFichierLogs - nom fichier utilisé : " << nomFichier << endl;
#endif
    bool answer = true;
    bool verifHoraire;
    Heure heureFin = horaireDepart + Ecart_Param_T;
    if (horaireDepart == NO_HEURE)
    {
        verifHoraire = false;
    }
    else
    {
        verifHoraire = true;
    }

    ifstream myfile(nomFichier);

    //Tests pour vérifier la validité du flux
    if (myfile.is_open())
    {
#ifdef MAP
        clog << "Graphe : méthode ChargerFichierLogs - fichier valide." << endl;
#endif
        // Tampon courant
        string line;
        int nbLigne = 0;

        //Données modifiées à chaque tour
        string IP;
        string userLogName;
        string userName;
        string garbage;
        string date;
        string fuseau;
        string requete;
        string code;
        string quantiteData;
        string referer;
        string useragent;
#ifdef TIMEMAP
        clog << "Graphe : méthode ChargerFichierLogs - Mesurage du temps activé." << endl;
        clock_t t = clock();
#endif
        while (getline(myfile, line))
        {
            //Prévention des fichiers vides.
            if (line == "")
            {
                cout << "ERREUR : Fichier vide. Veuillez fournir des donnees au programme." << endl;
                answer = false;
                return answer;
            }
            // On créé un flux à partir de la ligne
            stringstream lineStream;
            lineStream.str(line);

            //192.168.0.4 - - [08/Sep/2012:11:19:13 +0200] "GET /SiteWebIF/Intranet-etudiant.php HTTP/1.1"
            // 200 3775 "-" "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1
            // (KHTML, like Gecko) Chrome/21.0.1180.89 Safari/537.1"

            //On parse les arguments un par un
            getline(lineStream, IP, ' ');
            getline(lineStream, userLogName, ' ');
            getline(lineStream, userName, ' ');
            getline(lineStream, garbage, '[');
            getline(lineStream, date, ' ');
            getline(lineStream, fuseau, ']');
            getline(lineStream, garbage, '"');
            getline(lineStream, requete, '"');
            getline(lineStream, garbage, ' ');
            getline(lineStream, code, ' ');
            getline(lineStream, quantiteData, ' ');
            getline(lineStream, garbage, '"');
            getline(lineStream, referer, '"');
            getline(lineStream, garbage, '"');
            getline(lineStream, useragent, '"');

            nbLigne++;
#ifdef MAPV
            clog << "==== LIGNE N°" << nbLigne << " ====" << endl;
            clog << " IP : /" << IP << "/" << endl;
            clog << " userLogName :/" << userLogName << "/" << endl;
            clog << " userName :/" << userName << "/" << endl;
            clog << " date : /" << date << "/" << endl;
            clog << " fuseau :/" << fuseau << "/" << endl;
            clog << " requete :/" << requete << "/" << endl;
            clog << " code : /" << code << "/" << endl;
            clog << " quantiteData :/" << quantiteData << "/" << endl;
            clog << " referer :/" << referer << "/" << endl;
            clog << " useragent : /" << useragent << "/" << endl;
            clog << "==== FIN LIGNE N°" << nbLigne << " ====" << endl;
#endif

            // ----------- Traitement des informations supplémentaires -----------
            stringstream heureStream;
            heureStream.str(date);

            string heureH;
            string minH;
            string secH;
            getline(heureStream, garbage, ':');
            getline(heureStream, heureH, ':');
            getline(heureStream, minH, ':');
            getline(heureStream, secH, ':');

#ifdef MAPV
            clog << "==== Parsing des informations diverses ====" << endl;
            clog << " heureH : /" << heureH << "/" << endl;
            clog << " minH : /" << minH << "/" << endl;
            clog << " secH : /" << secH << "/" << endl;
            clog << "==== FIN Parsing de la requête N°" << nbLigne << " ====" << endl;
#endif
            // ----------- Traitement de la requête -----------
            stringstream requeteStream;
            requeteStream.str(requete);

            string code;
            string ressource;
            string versionHTTP;

            getline(requeteStream, code, ' ');
            getline(requeteStream, ressource, ' ');
            getline(requeteStream, versionHTTP);

            //Normalisation :
            string ressourceNormalise = normaliseURL(ressource); //Simplification possible
            string refererNormalise = normaliseURL(referer); //Simplification possible

#ifdef MAPV
            clog << "==== Parsing de la requête ====" << endl;
            clog << " code : /" << code << "/" << endl;
            clog << " ressource :/" << ressource << "/" << endl;
            clog << " ressourceNormalise :/" << ressourceNormalise << "/" << endl;
            clog << " versionHTTP :/" << versionHTTP << "/" << endl;
            clog << "==== FIN Parsing de la requête ====" << endl;
#endif

            Heure heureTMP = Heure(stoi(heureH), stoi(minH), stoi(secH));
            if (!verifHoraire || heureTMP.isInside(horaireDepart, heureFin))
            {
#ifdef MAPV
                clog << "Graphe : Parsing - Il n'y a pas de vérifications sur "
                        "l'horaire ou l'élement est dans l'horaire" << endl;
#endif
                if (!verifType || (ressourceEstBonType(ressourceNormalise)
                        && ressourceEstBonType(refererNormalise)))
                {
#ifdef MAPV
                    clog << "Graphe : Parsing - Il n'y a pas de vérifications " <<
                            "sur le type, ou le type correspond" << endl;
#endif
                    ajoutRessource(ressourceNormalise);

                    //Traitement du referer
                    ajoutRessourceReferer(refererNormalise, ressourceNormalise, heureTMP);
                }

            }

        }

#ifdef TIMEMAP
        clog << "Graphe : méthode ChargerFichierLogs - Mesurage du temps stoppé." << endl;
        clog << "Graphe : méthode ChargerFichierLogs - " << nbLigne << " lignes parsées." << endl;
        clog << "Graphe : méthode ChargerFichierLogs - temps de parsing : "
                << (((float) clock() - t) / CLOCKS_PER_SEC) << endl;
        clog << "Graphe : méthode ChargerFichierLogs - Fin de fichier rencontrée." << endl;
#endif
        myfile.close();
    }
    else
    {
        #ifdef MAP
        clog << "Graphe : méthode ChargerFichierLogs - Fichier invalide." << endl;
        #endif
        cout << nomFichier << ": Permission denied or file do not exist" << endl;
        answer = false;
        return answer;
    }
    return answer;
}//----- Fin de ChargerFichierLogs

bool Graphe::ressourceEstBonType(string URL)
// Algorithme : pas d'algo particulier
{
    bool answer = true;

    string typeTMP = Ressource::getType(URL);
    for (string element : types_interdits)
    {
        if (typeTMP == element)
        {
            return false;
        }
    }
    return answer;
}//----- Fin de ressourceEstBonType

bool Graphe::ajoutRessource(string URL)
// Algorithme : pas d'algo particulier
{
#ifdef MAPV
    clog << "Graphe : méthode ajoutRessource - URL : " << URL << endl;
#endif

    //On créé un itérateur
    listeResTYPE::iterator it;
    bool answer = false;

    //On vérifie si URL existe dans la base.
    it = listeRessources.find(URL);

    //Si on a touché la fin de la map, la ressource n'est pas présente.
    if (it == listeRessources.end())
    {
#ifdef MAP
        clog << "Graphe : méthode ajoutRessource - ressource n'existe pas. Ajout de : " << URL << endl;
#endif
        //Insert renvoie une paire. On en prépare donc une pour retenir
        pair < listeResTYPE::iterator, bool> retour;

        //On la rajoute (Appelle double au constructeur par copie de Ressource, pourrait être amélioré)
        retour = listeRessources.insert(make_pair(URL, Ressource(URL, index)));
        index++;

        //La partie guache (l'iterator sur la map) puis la partie droite (La ressource) puis la fonction
        retour.first->second.ajoutVue();

        //On récupère si l'ajout a été fait correctement
        answer = retour.second;
    }
    else //Elle est déjà présente.
    {
#ifdef MAP
        clog << "Graphe : méthode ajoutRessource - ressource existe. "
                "Ajout d'une 1 vue en plus.URL: " << URL << endl;
#endif
#ifdef MAPV
        clog << "Graphe : affichage de la ressource : " << it->second << endl;
#endif
        it->second.ajoutVue();
    }

#ifdef MAPV
    clog << "Graphe : méthode ajoutRessource - Affichage de la map actuelle \n ==============" <<
            "====================" << endl;
    clog << *this << endl;
    clog << "================================== \nGraphe : méthode ajoutRessource - FIN " << endl;
#endif
    return answer;

} //----- Fin de ajoutRessource

bool Graphe::ajoutRessourceReferer(string URL, string URLcible, Heure heure)
// Algorithme : pas d'algo particulier
{
#ifdef MAPV
    clog << "Graphe : méthode ajoutRessourceReferer - URL : " << URL
            << " et URL Cible : " << URLcible << " à l'heure " << heure << endl;
#endif

    //On créé un itérateur
    listeResTYPE::iterator it;
    bool answer = true;

    //On vérifie si URLcible existe dans la base. Sinon, on le créé aussi.
    it = listeRessources.find(URL);

    //Si on a touché la fin de la map, la ressource n'est pas présente.
    if (it == listeRessources.end())
    {
#ifdef MAP
        clog << "Graphe : méthode ajoutRessourceReferer - La ressource referente "
                << URL << " n'existe pas. On l'ajoute. " << endl;
#endif
        //Insert renvoie une paire. On en prépare donc une pour retenir
        pair < listeResTYPE::iterator, bool> retour;

        //On la rajoute (Appelle double au constructeur par copie de Ressource, pourrait être amélioré)
        retour = listeRessources.insert(make_pair(URL, Ressource(URL, index)));
        index++;

        //On récupère si l'ajout a été fait correctement
        answer = retour.second;

        //on range dans l'itérateur général l'endroit où est notre nouveau referer.
        it = retour.first;
    }
    else //Elle est déjà présente.
    {
#ifdef MAP
        clog << "Graphe : méthode ajoutRessourceReferer - La ressource " << URL
                << " existe. On sait où elle se situe dans la MAP. " << endl;
#endif
#ifdef MAPV
        clog << "Graphe : affichage du referer : " << it->second << endl;
#endif
    }

    // On ajoute un Lien à URLcible pour aller à URL.
    it->second.ajoutLien(URLcible, heure);

#ifdef MAPV
    clog << *this << endl;
    clog << "Graphe : méthode ajoutRessourceReferer - FIN " << endl;
#endif

    return answer;
} //----- Fin de ajoutRessourceReferer

bool Graphe::GenererFichierGraph(const string nomFichier)
// Algorithme : pas d'algo particulier
{
#ifdef MAP
    clog << "Graphe : GenererFichierGraph - Génération du Fichier GraphViz." << endl;
#endif
    //On prévient que le fichier existe
    if(fileExists(nomFichier)) cout << nomFichier << ": this file already exists, overwriting ..." << endl;
    //Ouverture du fichier en mode écriture
    ofstream fichierGraphViz(nomFichier, ios::out | ios::trunc);
    // Manque la gestion des permissions + gestion de fichier déjà existant
    if (fichierGraphViz)
    {
        string out = "";

        // Ouverture en syntaxe GraphViz
        out += "digraph {\r\n";

        // Parcours de la map
        listeResTYPE::iterator it;
        listeLiensSortTYPE::iterator it2;

        // Ecriture des noeuds
        for (it = listeRessources.begin(); it != listeRessources.end(); ++it)
        {
#ifdef MAP
            clog << "Graphe : GenererFichierGraph - Ressource: " << it->first
                    << " ajoutée au fichier." << endl;
#endif
            out += "node" + to_string(it->second.getIndex()) + " [label=\""
                    + it->first + "\"];\r\n";
        }



        // Ecriture des liens
        for (it = listeRessources.begin(); it != listeRessources.end(); ++it)
        {
            if (it->second.getListeLiensSortants().size() != 0)
            {
#ifdef MAPV
                clog << "Graphe : GenererFichierGraph - Première boucle, itFirst: "
                        << it->first << " itSecond " << it->second << endl;
#endif
                for (it2 = it->second.getListeLiensSortants().begin();
                        it2 != it->second.getListeLiensSortants().end(); ++it2)
                {
#ifdef MAPV
                    clog << "Graphe : GenererFichierGraph - Deuxième boucle, Lien de "
                            << it->first << " jusqu'à: " << it2->first << " ajouté au fichier." << endl;
#endif
                    //Récupération de l'index de la page cible: NE FONCTIONNE PAS ...
                    listeResTYPE::iterator it3 = listeRessources.find(it2->first);
#ifdef MAPV
                    clog << "Graphe : GenererFichierGraph - Affichage de l'index cible "
                            << it3->first << " index: " << it3->second.getIndex() << endl;
#endif
                    if (it3 != listeRessources.end())
                    {
                        unsigned int ind = it3->second.getIndex();
                        out += "node" + to_string(it->second.getIndex()) + " -> node"
                                + to_string(ind) + " [label=\""
                                + to_string(it2->second.getnbUtilisation()) + "\"];\r\n";
                    }
                }
            }
        }
        // Fermeture de la syntaxe GraphViz
        out += "}\r\n";
#ifdef MAP
        clog << "Graphe : GenererFichierGraph - Ajout final au fichier : \n" << out << endl;
#endif
        fichierGraphViz << out;
        fichierGraphViz.close();
    }
    else cerr << "Erreur à l'ouverture !" << endl;
    return 1;
}//----- Fin de GenererFichierGraph

void Graphe::AfficherTop(int n)
// Algorithme : pas d'algo particulier
{
    //Déclaration pour simplifier
    typedef function<bool(pair<string, Ressource>, pair<string, Ressource>) > Comparator;

    //Pour trier
    Comparator compFunctor =
            [](pair<string, Ressource> elem1, pair<string, Ressource> elem2)
            {
                //Si 2 elements ont la même valeur, alors on trie par ordre alphabétique
                if (elem1.second.getnbVue() == elem2.second.getnbVue())
                {
                    return elem1.first <= elem2.first;
                }
                //Sinon on trie par nombre de vue (nb hits)
                return elem1.second.getnbVue() > elem2.second.getnbVue();
            };

    multiset<pair<string, Ressource>, Comparator> multiSetOfRessource(
                                                                      listeRessources.begin(), listeRessources.end(), compFunctor);

    //On affiche les n premiers
    multiset<pair<string, Ressource>, Comparator>::iterator it = multiSetOfRessource.begin();
    int i = 0;
    for (it; it != multiSetOfRessource.end() && i < n && it->second.getnbVue() > 0; ++it)
    {
        cout << it->first << " (" << it->second.getnbVue() << " hits)" << endl;
        i++;
    }
}//----- Fin de AfficherTop10

void Graphe::chargerConfig()
// Algorithme : pas d'algo particulier
{
    //On charge le fichier de config
    ifstream myfile(NOM_FICHIER_CONFIG);
    bool isCorrect = true;

#ifdef MAP
    clog << "Graphe : chargerConfig - Chargement du fichier de config : " <<
            NOM_FICHIER_CONFIG << endl;
#endif

    try
    {
        //Tests pour vérifier la validité du flux
        if (myfile.is_open())
        {
            //Exemple de fichier de configuration :
            //DOMAINE= http://intranet-if.insa-lyon.fr;
            //NB_TYPE_INTERDITS= 7;
            //TYPE_INTERDITS= css js bmp jpeg jpg png gif;
            //ECART_PARAM_T= 1 0 0;

            stringstream lineStream;
            string line;
            string garbage;
            string domaine;
            string nb_type_interditsSTR;
            int nb_type_interditsINT;
            string ecart_param_t_heure;
            string ecart_param_t_min;
            string ecart_param_t_sec;

            // On créé un flux à partir de la ligne
            getline(myfile, line);
            lineStream.str(line);

            //On parse les arguments un par un
            //Le domaine
            getline(lineStream, garbage, ' ');
            if (garbage != "DOMAINE=")
            {
                isCorrect = false;
            }
            getline(lineStream, domaine, ';');

#ifdef MAP
            clog << "==== Parsing des informations diverses du fichier de config ====" << endl;
            clog << " domaine : /" << domaine << "/" << endl;
#endif

            //Nb de types
            getline(myfile, line);
            lineStream.str(line);

            getline(lineStream, garbage, ' ');
            if (garbage != "NB_TYPE_INTERDITS=")
            {
                isCorrect = false;
            }
            getline(lineStream, nb_type_interditsSTR, ';');
            nb_type_interditsINT = stoi(nb_type_interditsSTR);

#ifdef MAP
            clog << " nb_type_interditsSTR : /" << nb_type_interditsSTR << "/" << endl;
            clog << " nb_type_interditsINT : /" << nb_type_interditsINT << "/" << endl;
#endif

            //types
            getline(myfile, line);
            lineStream.str(line);
            getline(lineStream, garbage, ' ');
            if (garbage != "TYPE_INTERDITS=")
            {
                isCorrect = false;
            }

            types_interdits.clear();
            for (int i = 0; i < nb_type_interditsINT - 1; i++)
            {
                getline(lineStream, garbage, ' ');
                types_interdits.push_back(garbage);
#ifdef MAP
                clog << " type courant ajouté : /" << garbage << "/" << endl;
#endif
            }
            getline(lineStream, garbage, ';');
            types_interdits.push_back(garbage);
#ifdef MAP
            clog << " type courant ajouté : /" << garbage << "/" << endl;
#endif

            //heure_ecart
            getline(myfile, line);
            lineStream.str(line);
            getline(lineStream, garbage, ' ');
            if (garbage != "ECART_PARAM_T=")
            {
                isCorrect = false;
            }

            getline(lineStream, ecart_param_t_heure, ' ');
            getline(lineStream, ecart_param_t_min, ' ');
            getline(lineStream, ecart_param_t_sec, ';');

#ifdef MAP
            clog << " ecart_param_t_heure : /" << ecart_param_t_heure << "/" << endl;
            clog << " ecart_param_t_min : /" << ecart_param_t_min << "/" << endl;
            clog << " ecart_param_t_sec : /" << ecart_param_t_sec << "/" << endl;
#endif

            if (!isCorrect)
            {
                types_interdits.clear();
                cerr << "ERREUR: Fichier de configuration incorrect. " <<
                        "Choix des paramètres par défaut." << endl;
            }
            else
            {
                //On range !
                Ecart_Param_T = Heure(stoi(ecart_param_t_heure),
                                      stoi(ecart_param_t_min), stoi(ecart_param_t_sec));
                Notre_domaine = domaine;
                nb_types_interdits = nb_type_interditsINT;

#ifdef MAP
                clog << "Graphe : chargerConfig - Le fichier de config a été "
                        "correctement chargé" << endl;
#endif
            }
        }
    }
    catch (const std::exception & Exp)
    {
        std::cerr << "ERREUR: Fichier de configuration impossible à ouvrir. Choix "
                "des paramètres par défaut." << endl;

        //On met les valuers par défaut
        nb_types_interdits = DEFAULT_NB_TYPE_INTERDITS;
        for (int i = 0; i < DEFAULT_NB_TYPE_INTERDITS; i++)
        {
            types_interdits.push_back(DEFAULT_TYPE_INTERDITS[i]);
        }
        Notre_domaine = DEFAULT_DOMAINE;
        Ecart_Param_T = DEFAULT_ECART_PARAM_T;
#ifdef MAP
        clog << "Graphe : chargerConfig - Le fichier de config n'a pas été chargé." << endl;
#endif
    }

#ifdef MAP
    clog << "Graphe : Configuration actuelle :" << endl;
    clog << "nb_types_interdits " << nb_types_interdits << endl;
    clog << "Taille Types interdits " << types_interdits.size() << endl;
    for (unsigned int i = 0; i != types_interdits.size(); i++)
    {
        clog << types_interdits[i] << endl;
    }
    clog << "Notre_domaine " << Notre_domaine << endl;
    clog << "Ecart_Param_T " << Ecart_Param_T << endl;
#endif
}

//------------------------------------------------- Surcharge d'opérateurs

ostream & operator<<(ostream & out, const Graphe & variable)
{
#ifdef MAP
    clog << "Graphe : méthode << - Affichage de la map actuelle " << endl;
#endif
    unordered_map<string, Ressource>::const_iterator itest;

    for (itest = variable.listeRessources.begin(); itest != variable.listeRessources.end(); ++itest)
    {
        cout << " URL : " << itest->first << endl;
        cout << " Détails ressource : " << itest->second << endl;
    }

    return out;
}

//-------------------------------------------- Constructeurs - destructeur

Graphe::Graphe(const Graphe & unGraphe)
// Algorithme : pas d'algo particulier
{
#ifdef MAP
    clog << "Appel au constructeur de copie de <Graphe>" << endl;
#endif
} //----- Fin de Graphe (constructeur de copie)

Graphe::Graphe()
// Algorithme : pas d'algo particulier
{
#ifdef MAP
    clog << "Appel au constructeur de <Graphe>" << endl;
#endif
    //On met les valuers par défaut
    nb_types_interdits = DEFAULT_NB_TYPE_INTERDITS;
    for (int i = 0; i < DEFAULT_NB_TYPE_INTERDITS; i++)
    {
        types_interdits.push_back(DEFAULT_TYPE_INTERDITS[i]);
    }
    Notre_domaine = DEFAULT_DOMAINE;
    Ecart_Param_T = DEFAULT_ECART_PARAM_T;
} //----- Fin de Graphe

Graphe::~Graphe()
// Algorithme : pas d'algo particulier
{
#ifdef MAP
    clog << "Appel au destructeur de <Graphe>" << endl;
#endif
} //----- Fin de ~Graphe


//------------------------------------------------------------------ PRIVE

//----------------------------------------------------- Méthodes protégées

//------------------------------------------------------- Méthodes privées

string Graphe::normaliseURL(string URL)
// Algorithme : commence par la fin pour trouver le dernier . de l'adresse.
{
#ifdef MAPV
    clog << "Graphe : méthode normaliseURL - URL envoyée : " << URL << endl;
#endif

    string answer = URL;

    //Permet de transformer l'URL en quelques chose de correcte :
    // Retirer le nom de domaine si c'est le notre
    // Supprimer les éventuelles variable passees en URL après un ? ou un ;
    // ======================================================================= TO DO =========================================================

    std::size_t found = URL.find(Notre_domaine);
    if (found != std::string::npos && found == 0) // si on a pas "rien" trouvé
    {
#ifdef MAPV
        clog << "Graphe : méthode normaliseURL - domaine local détecté jusqu'au caractère : " << found << endl;
#endif
        answer = answer.substr(Notre_domaine.length(), URL.length());
    }
    found = answer.find("?");
    if (found != std::string::npos) // si on a pas "rien" trouvé
    {
#ifdef MAPV
        clog << "Graphe : méthode normaliseURL - ? détecté au caractère : " << found << endl;
#endif
        answer = answer.substr(0, found);
    }
    found = answer.find(";");
    if (found != std::string::npos) // si on a pas "rien" trouvé
    {
#ifdef MAPV
        clog << "Graphe : méthode normaliseURL - ; détecté au caractère : " << found << endl;
#endif
        answer = answer.substr(0, found);
    }
#ifdef MAPV
    clog << "Graphe : méthode normaliseURL - URL retournée : " << answer << endl;
#endif
    return answer;
}
bool Graphe::fileExists(const string& filename)
{
    struct stat buf;
    if (stat(filename.c_str(), &buf) != -1)
    {
        return true;
    }
    return false;
}

