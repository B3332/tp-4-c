# --- Variables ----
RM = rm
ECHO = echo
COMP = g++
EDL = g++
RMFLAGS = -f
CFLAGS = -W -Wall -ansi -pedantic -g -std=c++11 -Wno-unused
LDFLAGS = 
INT = main.h Graphe.h Lien.h Ressource.h Heure.h
#Liste des .h qui seront ensuite modifi� en .cpp et .o pour g�n�rer les autres variables du makefile.
# Note : on doit pouvoir utiliser $(wildcard *.cpp)
REAL = $(INT:.h=.cpp)
OBJ=$(REAL:.cpp=.o)
#les librairies qui peuvnet nous �tres utiles
LIBS = # -lm -lpthread
# Le nom de l'executable ci dessous : 
EXE = exe
# Mode de DEBUG standard
DEBUG = debug
# Mode de DEBUG verbeux
DEBUGV = debugv
# Mode de DEBUG orient� calcul de temps
DEBUGT = debugt
EFFACE = clean
# Mode permettant la cr�ation du fichier graphviz automatiquement
GRAPHVIZ = dot
#Gestion du graphViz
FICGIN = ./test.dot
FICGOUT = test.png
#Cible Test
TEST = test

# ---- Phony ----
.PHONY = $(EFFACE)
#Permet d'�viter que le make chercher le fichier clean pour savoir si il est � jour ou non.

# ---- R�gles ---- (ici, exclusivement explicite)

bin : $(OBJ)
	$(COMP) $(FLAGS) $(OBJ) -o $(EXE)

# compilation
%.o:%.cpp
	$(COMP) $(CFLAGS) -c $< -o $@

# Construction de l'executable, par �ditions des liens
$(EXE) : $(OBJ)
	$(ECHO) "Edition des liens de <$(EXE)>"
	$(EDL) -o $(EXE) $(OBJ) $(LIBS)

#Generation du graphViz
${GRAPHVIZ} :
	${GRAPHVIZ} ${FICGIN} -Tpng -o ${FICGOUT} -v 2>&1

# Construction de l'executableen mode DEBUG, par �ditions des liens

$(DEBUG) : CFLAGS += -DMAP
$(DEBUG) : $(OBJ)
	$(ECHO) "Edition des liens de <$(DEBUG)>"
	$(EDL) -o $(DEBUG) $(OBJ) $(LIBS) 

$(DEBUGV) : CFLAGS += -DMAPV -DMAP
$(DEBUGV) : $(OBJ)
	$(ECHO) "Edition des liens de <$(DEBUGV)>"
	$(EDL) -o $(DEBUGV) $(OBJ) $(LIBS) 

$(DEBUGT) : CFLAGS += -DTIMEMAP
$(DEBUGT) : $(OBJ)
	$(ECHO) "Edition des liens de <$(DEBUGT)>"
	$(EDL) -o $(DEBUGT) $(OBJ) $(LIBS)

$(TEST) :
	./Tests/mktestDepuisRacine.sh

# Rm -f de tous les fichiers cr�� par le make file, donc l'executable, les .o et le core, en cas de dumped core du syst�me
$(EFFACE) :
	$(RM) $(RMFLAGS) $(EXE) $(DEBUG) $(DEBUGV)  $(DEBUGT) $(OBJ) ${FICGIN} ${FICGOUT} Core 

# Notes prises sur le Makefile
# Variables internes � Make : 
# $@ 	Le nom de la cible
# $< 	Le nom de la premi�re d�pendance
# $^ 	La liste des d�pendances
# $? 	La liste des d�pendances plus r�centes que la cible
# $* 	Le nom du fichier sans suffixe
