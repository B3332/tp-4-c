/*************************************************************************
                           Ressource  -  description
                             -------------------
    début                : Janvier - Février 2017
    copyright            : (C) 2016-2017 par FALCONIERI VINCENT - GIRAUD CORENTIN
    e-mail               : vincent.falconieri@insa-lyon.fr & corentin.giraud@insa-lyon.fr
 *************************************************************************/
//---------- Interface de la classe <Ressource> (fichier Ressource.h) ----------------
#if ! defined ( RESSOURCE_H )
#define RESSOURCE_H
//--------------------------------------------------- Interfaces utilisées
#include <string>
#include <unordered_map>
#include <utility>      // std::pair
#include "Lien.h"
//------------------------------------------------------------- Constantes
typedef std::unordered_map<string, Lien> listeLiensSortTYPE;
//------------------------------------------------------------------ Types
//------------------------------------------------------------------------
// Rôle de la classe <Ressource>
// Représente une page, son URL, son type de ressource lié, son nombre de Vue ...
// 
//------------------------------------------------------------------------
class Ressource {
    //----------------------------------------------------------------- PUBLIC
public:
    //----------------------------------------------------- Méthodes publiques
    bool ajoutLien(string URLPageCible, Heure heure = NO_HEURE);
    // Permet d'ajouter un lien sortant à la ressource sur laquelle est executée
    // Mode d'emploi :
    // URLPageCible : l'url de la page cible (au bout de la flèche du lien)
    // heure : l'heure à laquelle le lien a été utilisé
    // Contrat : pas de contrat

    bool ajoutVue();
    // Permet d'incrémenter le compteur de vue de la ressource (nb Hits)
    // Mode d'emploi : aucun
    // Contrat : pas de contrat

    listeLiensSortTYPE &  getListeLiensSortants();
    // Permet de fournir une référence vers la liste des Liens sortant de la ressource
    // Mode d'emploi : aucun
    // Contrat : pas de contrat

    unsigned int getIndex() const;
    // Permet de fournir le numéro d'index de la ressource (identifiant utilisable)
    // Mode d'emploi : aucun
    // Contrat : pas de contrat

    unsigned int getnbVue() const;
    // Permet de fournir le nombre de vues (hits) de la ressource
    // Mode d'emploi : aucun
    // Contrat : pas de contrat

    static string getType(string URL);
    // Permet de fournir le type de la ressource de l'URL passée en paramètre
    // (php, png, html, css, ico ...)
    // Mode d'emploi : l'URL à parser
    // Contrat : pas de contrat

    //------------------------------------------------- Surcharge d'opérateurs
    Ressource & operator=(const Ressource & unRessource);
    // Surcharge de l'opérateur d'égalité.

    friend ostream & operator<<(ostream & out, const Ressource & variable);
    // Surcharge de l'opérateur de sortie.
    // Surcharge de l'opérateur d'égalité. 

    friend ostream & operator<<(ostream & out, const Ressource & variable);
    // Surcharge de l'opérateur de sortie. 

    friend bool operator< (const Ressource &left, const Ressource &right);
    // Surcharge de l'opérateur inférieur. 

    
    //-------------------------------------------- Constructeurs - destructeur
    Ressource(const Ressource & unRessource);
    // constructeur de copie
    // Contrat : pas de contrat

    Ressource(string URL, unsigned int index);
    // Constructeur de la classe Ressource
    // Mode d'emploi :
    // l'URL désigne l'URL à laquelle la Ressource est disponible
    // L'index désigne un identifiant qu'on donne à la ressource
    // Contrat : pas de contrat

    virtual ~Ressource();
    // Destructeur de la classe
    // Contrat : pas de contrat

    //------------------------------------------------------------------ PRIVE
protected:
    //----------------------------------------------------- Méthodes protégées
private:
    //------------------------------------------------------- Méthodes privées
protected:
    //----------------------------------------------------- Attributs protégés
private:
    //------------------------------------------------------- Attributs privés
    string typeName; 
    //Note on pourrait lister les types possibles dans une enum, 
    // mais au risque d'en oublier un !
    int nbVue;
    //Le nombre de vue qu'a eu la page. (Hits)
    int index;
    //Un numéro identifiant la page
    unordered_map<string, Lien> listeLiensSortants;
    // Sa propre structure de donées pour stocker les liens vers d'autres pages
    //---------------------------------------------------------- Classes amies
    //-------------------------------------------------------- Classes privées
    //----------------------------------------------------------- Types privés
};
//---------------------------------------------- Types dépendants de <Ressource>
#endif // RESSOURCE_H
