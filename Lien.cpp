/*************************************************************************
                           Lien  -  description
                             -------------------
    début                : Janvier - Février 2017
    copyright            : (C) 2016-2017 par FALCONIERI VINCENT - GIRAUD CORENTIN
    e-mail               : vincent.falconieri@insa-lyon.fr & corentin.giraud@insa-lyon.fr
 *************************************************************************/
//---------- Réalisation de la classe <Lien> (fichier Lien.cpp) ------------
//---------------------------------------------------------------- INCLUDE
//-------------------------------------------------------- Include système
using namespace std;
#include <iostream>

//------------------------------------------------------ Include personnel
#include "Lien.h"
//------------------------------------------------------------- Constantes
//---------------------------------------------------- Variables de classe
//----------------------------------------------------------- Types privés
//----------------------------------------------------------------- PUBLIC
//-------------------------------------------------------- Fonctions amies
//----------------------------------------------------- Méthodes publiques

bool Lien::ajoutUtilisation(Heure heure)
// Algorithme : aucun
{
    this->nbUtilisations++;
    // Plus éventuellement rajouter une entrée pour le vecteur d'heures.
    return true;
} //----- Fin de Méthode

int Lien::getnbUtilisation() const
// Algorithme : aucun
{
    return nbUtilisations;
} //----- Fin de getnbUtilisation

//------------------------------------------------- Surcharge d'opérateurs
ostream & operator<<(ostream & out, const Lien & variable)
{
    out << "Ce lien a été utilisé " << variable.nbUtilisations << " fois.";
    return out;
}

//-------------------------------------------- Constructeurs - destructeur
Lien::Lien(const Lien & unLien)
// Algorithme : aucun
{
#ifdef MAPV
    clog << "Appel au constructeur de copie de <Lien>" << endl;
#endif
    this->nbUtilisations = unLien.nbUtilisations;
} //----- Fin de Lien (constructeur de copie)

Lien::Lien(int nbUtilisations)
// Algorithme : aucun
{
#ifdef MAPV
    clog << "Appel au constructeur de <Lien>" << endl;
#endif
    this->nbUtilisations = nbUtilisations;
} //----- Fin de Lien

Lien::~Lien()
// Algorithme : aucun
{
#ifdef MAPV
    clog << "Appel au destructeur de <Lien>" << endl;
#endif
} //----- Fin de ~Lien

//------------------------------------------------------------------ PRIVE
//----------------------------------------------------- Méthodes protégées
//------------------------------------------------------- Méthodes privées